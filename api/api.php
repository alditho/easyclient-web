<?php
//Requerir una sola vez el modelo Api
require_once "../models/User.php";
require_once "../models/Request.php";

//instancia de Api
$user=new User();
$request=new Request();

//0 solicitud rechazada
//1 solicitud aceptada
//2 solicitud pendiente



switch ($_GET["op"]) {

    //iniciar sesión
    case 'login':

        //json
        $json_array=json_decode(file_get_contents('php://input', true), true);

        //obtener username json
        $username=$json_array["username"];
        $notificationToken=$json_array["notification_token"];
       
        $getUser=$user->getUserDb($username);
        if (empty($getUser)) {//si no se obtiene nada

            $registerUser=$user->registerUser(strtolower($username),$notificationToken); //se registro nuevo usuario.
            if($registerUser){
              $response = array("server_response"=>"success", "user_id"=>$registerUser,"username"=>$username);
              print_r(json_encode($response));
            }

        }else{//si se obtiene usuario se actualiza token de notificación

            $update=$user->updateNotificationToken(strtolower($username),$notificationToken);
                $response = array("server_response"=>"success", "user_id"=>$getUser["USER_ID"], "username"=>$getUser["username"]);
                print_r(json_encode($response)); 

        }

    break;


    //registrar solicitud
    case 'registerRequest':

        //json
        $json_array=json_decode(file_get_contents('php://input', true), true);

        //obtener username json
        $amount=$json_array["amount"];
        $age=$json_array["age"];
        $creditCard=$json_array["card"];
        $months=$json_array["months"];
        $user=$json_array["user_id"];

        $register=$request->registerRequest($amount,$age,$creditCard,$months,2,$user);
        if($register){
            $response = array("server_response"=>"success", "request"=>"registered");
            print_r(json_encode($response));       
        }

    break;


    //obtener solicitudes pendientes
    case 'getRequest':
    
        $user=$_GET["user_id"];

        $getRequest=$request->getRequestPending($user);
        if(mysqli_num_rows($getRequest) == 0 ){ //Si no se obtienen solicitudes
           $response = array("server_response"=>"failure", "cause"=>"request_not_found");
            print_r(json_encode($response));
        }elseif(mysqli_num_rows($getRequest) > 0 ){//Si se obtienen solicitudes

            $request = array();
            //Se llena arreglo de request
            while ($reg=$getRequest->fetch_object()){
                $request[]= array("request_id"=>$reg->REQUEST_ID,"amount"=>$reg->amount,"age"=>$reg->age,"credit_card"=>$reg->creditCard,
                            "months"=>$reg->months,"status"=>$reg->status);
            }

            //respuesta correcta   
            $respuesta = array("server_response"=>"success","request"=>$request);
            print_r(json_encode($respuesta)); 
        }



    break;


    //obtener solicitudes aceptadas y rechazadas
    case 'getRecordRequest':
    
        $user=$_GET["user_id"];

        $getRequest=$request->getRecordRequest($user);
        if(mysqli_num_rows($getRequest) == 0 ){ //Si no se obtienen solicitudes
           $response = array("server_response"=>"failure", "cause"=>"request_not_found");
            print_r(json_encode($response));
        }elseif(mysqli_num_rows($getRequest) > 0 ){//Si se obtienen solicitudes

            $request = array();
            //Se llena arreglo de request
            while ($reg=$getRequest->fetch_object()){
                $request[]= array("request_id"=>$reg->REQUEST_ID,"amount"=>$reg->amount,"age"=>$reg->age,"credit_card"=>$reg->creditCard,
                            "months"=>$reg->months,"status"=>$reg->status);
            }

            //respuesta correcta   
            $response = array("server_response"=>"success","request"=>$request);
            print_r(json_encode($response)); 
        }



    break;


    //procesar solicitudes
    case 'updateRequest':

    $process=$request->getRequestPendingProcess();
    while ($reg=$process->fetch_object()){
        
        if($reg->age>=20 && $reg->creditCard==1){//si tiene mas de 20 y tarjeta de credito, la solicitud es aceptada
            $getRequest=$request->updateRequest($reg->REQUEST_ID,1);
        }else{// de lo contrario es rechazada
            $getRequest=$request->updateRequest($reg->REQUEST_ID,0);
        }

       //se envia notificación para actualizar información
       $message = array("update"=>"request");
       $notification = array("title" => "EasyCredit",
       "body" => "Se actualizo el estado de tu solicitud","sound" => "default");
       $message_status = $user->send_notification($reg->notificationToken, $message , $notification);
    }


    break;

    //procesar 1 solicitud
    case 'updateOneRequest':

    //se obtiene id de solicitud a procesar
    $next=$request->getNextUpdate();
    if($next["age"]>=20 && $next["creditCard"]==1){ //si cumple con esto se acepta la solicitud
        $update=$request->updateRequest($next["REQUEST_ID"],1);//metodo para aceptar solicitud        
    }else{
        $update=$request->updateRequest($next["REQUEST_ID"],0); //metodo para rechazar solicitud
    }

    //se envia notificación para actualizar información
    $message = array("update"=>"request");
    $notification = array("title" => "EasyCredit",
    "body" => "Se actualizo el estado de tu solicitud","sound" => "default");
    $message_status = $user->send_notification($next["notificationToken"], $message , $notification);

    break;

    //cantidad de solicitudes pendientes
    case 'countPendingRequest':

      $user=$_GET["user_id"];
      $pending=$request->countPendingRequest($user);

      $response = array("server_response"=>"success","pending"=>$pending["pending"]);
      print_r(json_encode($response)); 

    break;


 
}//Termina switch

?>