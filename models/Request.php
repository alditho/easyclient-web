<?php 
//Conexión a base de datos
require "../config/connection.php";

//Clase Request
Class Request{

    //Implementamos constructor 
    public function __construct(){

    }

    //registrar solicitud
    public function registerRequest($amount,$age,$card,$months,$status,$user){
        $sql="INSERT INTO request(amount,age,creditCard,months,status,FK_USER_ID) VALUES ('$amount','$age','$card','$months','$status','$user')";
        return ejecutarConsulta_retornarID($sql);
    }

    //obtener solicitudes pendientes
    public function getRequestPending($user){
        $sql="SELECT * FROM request WHERE FK_USER_ID='$user' AND status='2' ORDER BY REQUEST_ID DESC";
        return ejecutarConsulta($sql);
    }

    //obtener solicitudes aceptas y rechazadas
    public function getRecordRequest($user){
        $sql="SELECT * FROM request WHERE FK_USER_ID='$user' AND status='0' OR FK_USER_ID='$user' AND status='1' ORDER BY REQUEST_ID DESC";
        return ejecutarConsulta($sql);
    }

    //obtener todas las solicitudes pendientes
    public function getRequestPendingProcess(){
        $sql="SELECT REQUEST_ID,notificationToken,age,creditCard,status FROM request
        JOIN user ON request.FK_USER_ID=user.USER_ID
        WHERE status='2'";
        return ejecutarConsulta($sql);
    }

    //actualizar solicitud
    public function updateRequest($request,$status){
        $sql="UPDATE request SET status='$status' WHERE REQUEST_ID='$request'";
        return ejecutarConsulta_retornarID($sql);
    }

    //cantidad de solicitudes pendientes
    public function countPendingRequest($user){
        $sql="SELECT COUNT(status) as pending FROM request WHERE status=2 AND FK_USER_ID='$user'";
        return ejecutarConsultaSimpleFila($sql);
    }

    //obtener solicitud que se aceptara o rechazara
    public function getNextUpdate(){
        $sql="SELECT * FROM request 
        JOIN user ON request.FK_USER_ID=user.USER_ID
        WHERE status= 2 ORDER BY REQUEST_ID ASC LIMIT 1";
        return ejecutarConsultaSimpleFila($sql);
    }







}//Termina clase Request

?>