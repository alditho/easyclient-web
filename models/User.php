<?php 
//Conexión a base de datos
require "../config/connection.php";

//Clase User
Class User{

    //Implementamos constructor 
    //usuario
    public function __construct(){

    }

    //obtener usuario
    public function getUserDb($username){
        $sql= "SELECT * FROM user WHERE username='$username'";
        return ejecutarConsultaSimpleFila($sql);
    }

    //registrar usuario
    public function registerUser($username,$token){
        $sql="INSERT INTO user(username,notificationToken) VALUES ('$username','$token')";
        return ejecutarConsulta_retornarID($sql);
    }

    //actualizar token de notificacion cuando inicia sesión
    public function updateNotificationToken($user,$token){
        $sql="UPDATE user SET notificationToken='$token' WHERE username='$user'";
        return ejecutarConsulta_retornarID($sql);
    }


    //PUSH NOTIFICATIONS
    public function send_notification($token, $message, $notification){
    
            $url = 'https://fcm.googleapis.com/fcm/send';
            $fields = array(
                 'to' => $token,
                // 'priority':'high',
                 'notification' => $notification,
                 'data' => $message
                );
            $headers = array(
                'Authorization:key=AAAAm9GsLZk:APA91bG_OxLwdMpW9C6xLkgl-TcudNF0Qoxjp8XUccv86WsPwK-YMHV0-j2LamuIo9mpMzwwByuv2-qmQ4FckszPW6HksEFw2at__WHcpseLjYruhAqfoda3sonXcBhXUY0KrZ9ny4Gu',
                'Content-Type: application/json'
                );
    
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $url);
           curl_setopt($ch, CURLOPT_POST, true);
           curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
           curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
           curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
           curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
           curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
           $result = curl_exec($ch);           
           if ($result === FALSE) {
               die('Curl failed: ' . curl_error($ch));
           }
           curl_close($ch);
    
           return $result;
    
           
     
    }
    

}//Termina clase Usuario

?>